CREATE TABLE Ideas(Id TEXT, Thesis TEXT, Antithesis TEXT, Synthesis TEXT, InterestingForTA TEXT, Appropriate TEXT, EffortEstimate TEXT, Comment TEXT);
INSERT INTO Ideas(Id, Thesis, Antithesis, Synthesis, InterestingForTA, Appropriate, EffortEstimate, Comment) VALUES('1', 'BPM can help your company get faster.', 'BPM is too expensive for
companies that are not too big to fail.
', 'With low-code solutions like
Zapier or Huginn, even small companies

can afford automation.
', 'Maybe', 'Yes', 'High', 'Effort estimate is high because we
need to create code samples as well.
');
INSERT INTO Ideas(Id, Thesis, Antithesis, Synthesis) VALUES('2', 'Low-code automation platforms
turn ordinary chaos into an automated

one (which is worse) because they

lack proper testing and deployment

tools.
', 'Low-code automation works
fine for many companies.
', 'Here are 7 things you can
do in order to develop, test,

deploy, and monitor your low-code

automation systems like a boss.
');
-- Ignoring empty record in Ideas table
