# Make the script stop on any error
set -e

# Constants (start)
BASE_DIR=`pwd`
DB_FILE="target/sample-2.db"
FLAT_FILES="$BASE_DIR/src/ideas.rec"
TARGET_DIR="$BASE_DIR/target"
SQL_FILE="$TARGET_DIR/sql.sql"
LOG_FILE="$TARGET_DIR/sqlite.log"
# Template for new files
# FLAT_FILES="$FLAT_FILES $BASE_DIR/src/sdata/<filename>"
# Constants (end)

echo "'$PRJ_NAME' Project"
echo "Updating everything we can update"
echo "Deleting the database file '$DB_FILE' if it exists..."
rm $DB_FILE || true
echo "Done"
echo "Creating an empty database file '$DB_FILE'..."
sqlite3 $DB_FILE ""
echo "Done"
echo "Importing flat file data into a SQLite database file..."

rm $SQL_FILE || true

cat $FLAT_FILES | \
python3 -m rec2sqlite > $SQL_FILE

rm $LOG_FILE || true

sqlite3 $DB_FILE < $SQL_FILE >> $LOG_FILE 2>&1

echo "Done"
