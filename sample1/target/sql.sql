CREATE TABLE Ideas(Id TEXT, Desc TEXT);
INSERT INTO Ideas(Id, Desc) VALUES('1', 'Form.Ui');
INSERT INTO Ideas(Id, Desc) VALUES('2', 'TypeForm');
INSERT INTO Ideas(Id, Desc) VALUES('3', 'JSF');
-- Ignoring empty record in Ideas table
CREATE TABLE Ideas2(Id TEXT, Desc TEXT, EaseOfUse TEXT, Costs TEXT, SupportedByCamunda TEXT);
INSERT INTO Ideas2(Id, Desc, EaseOfUse, Costs, SupportedByCamunda) VALUES('1', 'Form.Ui', 'Unknown', 'Medium to Low', 'No');
INSERT INTO Ideas2(Id, Desc, EaseOfUse, Costs, SupportedByCamunda) VALUES('2', 'TypeForm', 'High', 'Medium to Low', 'No');
INSERT INTO Ideas2(Id, Desc, EaseOfUse, Costs, SupportedByCamunda) VALUES('3', 'JSF', 'Medium', 'High', 'No');
