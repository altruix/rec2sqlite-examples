-- -*- mode: sql; coding: utf-8 -*-
CREATE VIEW MyView AS
SELECT ProAndContra.StatementId,
       S.[Desc] AS Statement,
       A.[Desc] AS Argument,
       ProAndContra.Relation AS [For or against?]
  FROM ProAndContra
       LEFT JOIN
       Statements S ON ProAndContra.StatementId = S.Id
       LEFT JOIN
       Statements A ON ProAndContra.ArgumentId = A.Id
 WHERE ProAndContra.StatementId = "1";
