CREATE TABLE Statements(Id TEXT, Desc TEXT);
INSERT INTO Statements(Id, Desc) VALUES('1', 'You should sell your gold
and buy Bitcoin instead.
');
INSERT INTO Statements(Id, Desc) VALUES('2', 'All the gazillionaires
(MicroStrategy, Paul Tudor Jones)

are buying Bitcoin.
');
INSERT INTO Statements(Id, Desc) VALUES('3', 'People say Bitcoin is
inconfiscatable.
');
INSERT INTO Statements(Id, Desc) VALUES('4', 'Bitcoin is the Libertarian
dream come true.
');
INSERT INTO Statements(Id, Desc) VALUES('5', 'The government can impose
anti-money laundering regulations

on Bitcoin like it already did

with KYC/ALM. And all banks will

comply.
');
INSERT INTO Statements(Id, Desc) VALUES('6', 'Bitcoin cannot be used for
paying for stuff because it is

too volatile.
');
INSERT INTO Statements(Id, Desc) VALUES('7', 'When you want to buy
Bitcoin, it is your FOMO

talking.
');
CREATE TABLE ProAndContra(StatementId TEXT, ArgumentId TEXT, Relation TEXT, Comment TEXT);
INSERT INTO ProAndContra(StatementId, ArgumentId, Relation) VALUES('1', '2', '+');
INSERT INTO ProAndContra(StatementId, ArgumentId, Relation) VALUES('1', '3', '+');
INSERT INTO ProAndContra(StatementId, ArgumentId, Relation) VALUES('1', '4', '+');
INSERT INTO ProAndContra(StatementId, ArgumentId, Relation) VALUES('1', '5', '-');
INSERT INTO ProAndContra(StatementId, ArgumentId, Relation) VALUES('1', '6', '-');
INSERT INTO ProAndContra(StatementId, ArgumentId, Relation) VALUES('1', '7', '-');
