import csv
import os
import fileinput

class Record:
    def __init__(self):
        self.connectorId = ""
        self.storyElementId = ""
        self.inversionComment = ""
        self.connectorDesc = ""
        self.storyElementType = ""
        self.storyElementDesc = ""
        self.inversionId = ""

class Connector:
    def __init__(self, conn_id, conn_desc):
        self.conn_id = conn_id
        self.desc = conn_desc

class StoryElement:
    def __init__(self, story_element_id, storyElementType, storyElementDesc):
        self.story_element_id = story_element_id
        self.story_element_type = storyElementType
        self.story_element_desc = storyElementDesc
        
class Transformer:
    def __init__(self):
        self.rows = []

    def process_line(self, line):
        pass

    def clean_str(self, txt):
        clean_txt = txt
        nl = os.linesep
        clean_txt = clean_txt.replace(nl+nl, nl)
        clean_txt = clean_txt.replace(nl, "<br/>")
        return clean_txt

    def extract_connectors(self):
        connectors = []
        processed_ids = [""]
        for cur_row in self.rows:
            cur_id = cur_row.connectorId
            if cur_id not in processed_ids:
                processed_ids.append(cur_id)
                cur_conn = Connector(cur_id, cur_row.connectorDesc)
                connectors.append(cur_conn)
        return connectors

    def extract_story_elements(self):
        story_elements = []
        processed_ids = [""]
        for cur_row in self.rows:
            cur_id = cur_row.storyElementId
            if cur_id not in processed_ids:
                processed_ids.append(cur_id)
                cur_story_element = StoryElement(cur_id, cur_row.storyElementType, cur_row.storyElementDesc)
                story_elements.append(cur_story_element)
        return story_elements

    def extract_inversions(self):
        inversions = []
        for cur_row in self.rows:
            if cur_row.connectorId and cur_row.storyElementId:
                inversions.append(cur_row)
        return inversions
    
    def process_row(self, row):
        record = Record()
        record.connectorId = row[0]
        record.storyElementId = row[1]
        record.inversionComment = row[2]
        record.connectorDesc = row[3]
        record.storyElementType = row[4]
        record.storyElementDesc = row[5]
        record.inversionId = row[6]
        self.rows.append(record)

    def render_conn(self, conn):
        pts = []
        pts.append("\t\t")
        pts.append("\"")
        pts.append(conn.conn_id)
        pts.append("\" [label=<")
        pts.append("<b>")
        pts.append(conn.conn_id) 
        pts.append("</b><br/>")
        conn_desc = self.clean_str(conn.desc)
        pts.append(conn_desc)
        pts.append(">, shape=box]")
        return "".join(pts)

    def render_story_element(self, story_element):
        pts = []
        pts.append("\t\t")
        pts.append("\"")
        pts.append(story_element.story_element_id)
        pts.append("\" [label=<")
        pts.append("<b>")
        pts.append(story_element.story_element_id)
        pts.append("</b><br/>")
        pts.append(story_element.story_element_type)
        pts.append("<br/>")
        story_element_desc = self.clean_str(story_element.story_element_desc)
        pts.append(story_element_desc)
        pts.append(">, shape=box]")
        return "".join(pts)

    def render_inversion(self, inversion, inv_nr):
        pts = []
        pts.append("\t")
        pts.append("i")
        pts.append(str(inv_nr))
        pts.append(" [shape=box, label=<")
        pts.append("<b>Inversion")
        if inversion.inversionId:
            pts.append(" ")
            pts.append(inversion.inversionId)
        pts.append("</b><br/>")
#        pts.append("Увидеть знакомое всем так,<br/> как на это ещё никто не<br/>смотрел, и заметить в нём то,<br/>что прежде никто не видел.<br/><br/>")
        pts.append(self.clean_str(inversion.inversionComment))
        pts.append(">]")
        return "".join(pts)

    def render_edge_ta_to_conn(self, conn):
        pts = []
        pts.append("\t")
        pts.append("ta -> \"")
        pts.append(conn.conn_id)
        pts.append("\"")
        return "".join(pts)
    def render_inversion_edge(self, inversion, inv_nr):
        pts = []
        pts.append("\t")
        pts.append("\"")
        pts.append(inversion.connectorId) 
        pts.append("\" -> i")
        pts.append(str(inv_nr))
        pts.append(" -> \"")
        pts.append(inversion.storyElementId)
        pts.append("\"")
        return "".join(pts)
    def create_dot(self):
        pts = []
        pts.append("digraph G {")
        pts.append("\trankdir=LR;")
        pts.append("\tgraph [fontname = \"Courier Prime\"];")
        pts.append("\tnode [fontname = \"Courier Prime\"];")
        pts.append("\tedge [fontname = \"Courier Prime\"];")

        # Create a connector subgraph

        pts.append("\tsubgraph cluster0 {")
        pts.append("\t\tlabel=<<b>Connectors</b><br/>")
        pts.append("What people already know about your project<br/>Universal knowledge.")
        pts.append("<br/>This what makes your content recognizable.")
        pts.append(">")
        # Get connectors
        connectors = self.extract_connectors()
        # Render connectors
        for cur_conn in connectors:
            pts.append(self.render_conn(cur_conn))
        pts.append("\t}")

        # TODO: Create "Our story" subgraph

        pts.append("\tsubgraph cluster1 {")
        pts.append("\t\tlabel=\"Your Story (e. g. blog post)\"")
        # Read the story elements
        story_elements = self.extract_story_elements()
        # Render story elements
        for cur_story_element in story_elements:
            pts.append(self.render_story_element(cur_story_element))
        pts.append("\t}")
        # Create Target Audience box (start)
        pts.append("\tta [shape=box, label=<<b>Target Audience</b><br/>People whom we want<br/>to sell Camunda>]")  
        # Create Target Audience box (end)
        # TODO: Create edges (start)
        # Connect target audience to every connector (start)
        for cur_conn in connectors:
            pts.append(self.render_edge_ta_to_conn(cur_conn))
        # Connect target audience to every connector (end)
        # Render inversions (start)
        inversions = self.extract_inversions()
        inv_counter = 1
        for cur_inversion in inversions:
            pts.append(self.render_inversion(cur_inversion, inv_counter))
            inv_counter = inv_counter + 1
        # Render inversions (end)
        # Render edges between inversions, story elements, and connectors (start)
        inv_counter = 1
        for cur_inversion in inversions:
            pts.append(self.render_inversion_edge(cur_inversion, inv_counter))
            inv_counter = inv_counter + 1
        # Render edges between inversions, story elements, and connectors (end)
        # Create edges (end)
        pts.append("}")
        return "\n".join(pts)

transformer = Transformer()

with open('target/ConnectorInversionDiagramData.csv', 'r') as file:
    reader = csv.reader(file, delimiter = ',')
    reading_header = True
    for row in reader:
        if not reading_header:
            transformer.process_row(row)
        else:
            reading_header = False

print(transformer.create_dot())
