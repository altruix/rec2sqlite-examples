CREATE TABLE ciConnectors(Id TEXT, Desc TEXT);
INSERT INTO ciConnectors(Id, Desc) VALUES('C-1', 'Camunda in particular and
BPM in general is used in boring

fields such as finance and

insurance.
');
CREATE TABLE ciStoryElements(Id TEXT, Type TEXT, Desc TEXT, OffChart TEXT);
INSERT INTO ciStoryElements(Id, Desc) VALUES('SE-1', 'Camunda can make your sex
life more efficient.
');
INSERT INTO ciStoryElements(Id, Desc) VALUES('SE-2', 'With Camunda, Vito Corleone
would have been much more

successful.
');
CREATE TABLE ciInversions(Id TEXT, ConnId TEXT, SeId TEXT, Comment TEXT, OffChart TEXT);
INSERT INTO ciInversions(ConnId, SeId, Comment) VALUES('C-1', 'SE-1', 'Everything is a process,
including relationships.
');
INSERT INTO ciInversions(ConnId, SeId, Comment) VALUES('C-1', 'SE-2', 'High-profile criminals
use something like Camunda, and

if they are not, they should.
');
